QUICKSTART GUIDE

SENSEI Conversational Repository


1. INSTALLATION

The server needs to run Mongo DB and Apache Tomcat.  (If Mongo DB is
running on a different machine, you will need to edit the
configuration file unpacked from the repository.war file.)


Find the repository.war file and deploy it as an Apache Tomcat
servlet.

EITHER

copy it to Tomcat's "webapps" directory and restart or start the
Tomcat server

OR

Use Tomcat's browser interface to deploy the repository.war file.


2. TESTING

The repository URL will be as follows:

http://<host>:<port>/repository

where the Tomcat server is running on port <port> on machine <host>.
You can use "localhost" for <host> from the machine on which Tomcat is
running, or if you use ssh tunnelling to connect a port on your
machine to the Tomcat port on a remote machine.


Test that the service is running with the following GET URLs (from a
web browser or curl):

http://<host>:<port>/repository?_wadl

(to return the WADL document)

http://<host>:<port>/repository/documents

(to get a list of all documents in the service)


Further details are given in the repository manual (PDF).
