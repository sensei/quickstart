The deliverad software is a Maven project. To load the project you can use any IDE such as NetBeans.
To load it using NewBeans follow these steps:

1. File
2. --> Open Project, here a window opens where you need to navigate to the director where the software is
3. Select the project and say "Open Project"
4. Your project is loaded to NetBeans

The softare comes with "clustering", "cluster labeling" and "summarization".

We have put these in a single pipeline. For this look at implementation within the package: "senseiprototypetools". You can run these pipelines and results will be saved under "articleComments" within
the same project. By editing the pipelines you can change the destination of the results as well as the inputs (news articles and comments). 
